#include <MsgBoxConstants.au3>
#include <FileConstants.au3>
#include <InetConstants.au3>
#include <WinAPIFiles.au3>
#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <ListViewConstants.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <GuiListView.au3>
#include <File.au3>
#include <Array.au3>
#include <Date.au3>
#include <WinAPI.au3>

Global $iSilentIs = 0

If $CmdLine[0] > 0 Then
	Switch $CmdLine[1]
		Case "/epg"
			Global $iSilentIs = 1
			Step4GetAndMerge()
		Case Else
			_WinAPI_AttachConsole()
			$hConsole = _WinAPI_GetStdHandle(1)
			_WinAPI_WriteConsole($hConsole, "Wrong parameter. Only /epg works = Get and merge EPG data silently." & @CRLF)

	EndSwitch
	Exit
EndIf

#Region ### START Koda GUI section ### Form=
Global $Form1 = GUICreate("IPTV to MP tools", 618, 479, -1, -1, $WS_OVERLAPPEDWINDOW)
Global $MenuFile = GUICtrlCreateMenu("&File")
Global $MenuIFileExit = GUICtrlCreateMenuItem("Exit", $MenuFile)
Global $MenuHelp = GUICtrlCreateMenu("&Help")
Global $MenuHelpHelp = GUICtrlCreateMenuItem("?" & @TAB & "F1", $MenuHelp)
Global $MenuHelpAbout = GUICtrlCreateMenuItem("About", $MenuHelp)

Global $Group1 = GUICtrlCreateGroup("Step 1 - Choose one group of channels", 8, 24, 297, 289)
Global $ButtonStep1ChooseFile = GUICtrlCreateButton("Choose M3U File", 16, 104, 91, 25)
Global $LabelStep1ChosenFile = GUICtrlCreateLabel("Use file: ", 16, 40, 286, 65)
;~ Global $LabelStep1ChosenFile = GUICtrlCreateLabel("LabelStep1ChosenFile", 16, 40, 286, 65)
Global $CheckboxAutoChangeFolderFile = GUICtrlCreateCheckbox("Auto Files in steps 2 and 3", 120, 108, 169, 17)
GUICtrlSetState(-1, $GUI_CHECKED)
Global $ListViewGroupSelection = GUICtrlCreateListView("", 16, 128, 281, 137, -1, BitOR($WS_EX_CLIENTEDGE, $LVS_EX_CHECKBOXES))
_GUICtrlListView_AddColumn($ListViewGroupSelection, "No", 45)
_GUICtrlListView_AddColumn($ListViewGroupSelection, "Group", 250)
Global $ButtonStep1GetGroups = GUICtrlCreateButton("Get groups", 16, 264, 75, 25)
Global $ButtonStep1ChooseDestination = GUICtrlCreateButton("Choose Destination Folder", 96, 264, 131, 25)
Global $ButtonStep1SaveToFile = GUICtrlCreateButton("Save group", 232, 264, 67, 25)
Global $LabelStep1Status = GUICtrlCreateLabel("Status", 14, 290, 280, 17)
GUICtrlCreateGroup("", -99, -99, 1, 1)

Global $Group2 = GUICtrlCreateGroup("Step 2 - Get logos for channels", 8, 320, 297, 129)
Global $ButtonStep2ChooseFile = GUICtrlCreateButton("Choose M3U File", 16, 400, 91, 25)
Global $LabelStep2ChosenFileAndFolder = GUICtrlCreateLabel("Use file:" & @CRLF & "Save folder:", 16, 336, 286, 65)
;~ Global $LabelStep2ChosenFileAndFolder = GUICtrlCreateLabel("LabelStep2ChosenFileAndFolder", 16, 336, 286, 65)
Global $ButtonStep2SaveToFolder = GUICtrlCreateButton("Get logos", 248, 400, 51, 25)
Global $LabelStep2Status = GUICtrlCreateLabel("Status", 16, 424, 280, 17)
;~ Global $LabelStep2Status = GUICtrlCreateLabel("LabelStep2Status", 16, 424, 280, 17)
Global $ButtonStep2ChooseDestinationFolder = GUICtrlCreateButton("Choose Destination Folder", 112, 400, 131, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)

Global $Group3 = GUICtrlCreateGroup("Step 3 - Fix M3U to MP1 format", 312, 24, 297, 137)
Global $LabelStep3ChosenFile = GUICtrlCreateLabel("Use file:", 320, 40, 286, 65)
;Global $LabelStep3ChosenFile = GUICtrlCreateLabel("Use file:", 320, 40, 286, 65)
Global $ButtonStep3ChooseFile = GUICtrlCreateButton("Choose M3U File", 320, 112, 91, 25)
Global $ButtonStep3ChooseFolder = GUICtrlCreateButton("Choose Destination Folder", 416, 112, 131, 25)
Global $ButtonStep3SaveFile = GUICtrlCreateButton("Fix/Save", 552, 112, 50, 25)
Global $LabelStep3Status = GUICtrlCreateLabel("Status", 320, 136, 280, 17)
;Global $LabelStep3Status = GUICtrlCreateLabel("Status", 320, 136, 280, 17)
GUICtrlCreateGroup("", -99, -99, 1, 1)

Global $Group4 = GUICtrlCreateGroup("Step 4 - Get and merge EPG XML files", 312, 176, 297, 273)
Global $CheckboxMergeEPGXMLfiles = GUICtrlCreateCheckbox("Merge EPG XML files to one file", 320, 192, 281, 17)
Global $ListViewEPGXMLfiles = GUICtrlCreateListView("", 320, 216, 281, 137)
_GUICtrlListView_AddColumn($ListViewEPGXMLfiles, "Key", 35)
_GUICtrlListView_AddColumn($ListViewEPGXMLfiles, "URL", 550)
Global $ButtonStep4GetAndMerge = GUICtrlCreateButton("Get/Merge EPG XML", 456, 352, 115, 25)
Global $LabelStep4Status = GUICtrlCreateLabel("Status", 320, 384, 280, 57)
;~ Global $LabelStep4Status = GUICtrlCreateLabel("LabelStep4Status", 320, 384, 280, 57)
Global $ButtonStep4DestinationFolder = GUICtrlCreateButton("Coose Destination Folder", 320, 352, 131, 25)
GUICtrlCreateGroup("", -99, -99, 1, 1)

Global $Form1_AccelTable[1][2] = [["{F1}", $MenuHelpHelp]]
GUISetAccelerators($Form1_AccelTable)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

HotKeySet("{F1}", "ShowHelp")

;Global variables
Global $sThisNewFileHasBeenChosen = "None", $sThisNewFolderHasBeenChosen = "None"

Global $sINIfile = StringReplace(@ScriptName, StringRight(@ScriptName, 3), "ini")

Global $sFileStep1ChooseM3Ufile
Global $sFolderStep1SaveGroup
Global $aUniqueGroupsToListView[1][2]

Global $sFileStep2GetLogos
Global $sFolderStep2SaveLogos

Global $sFileStep3FixM3U
Global $sFolderStep3SaveFixM3U

Global $sFolderStep4EPGXML
Global $sFileStep4EPGXML
Global $aEPGXMLfiles

Global $sStatusTemp

main()

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		Case $MenuIFileExit
			Exit
		Case $MenuHelpAbout
			ShowAbout()
		Case $MenuHelpHelp
			ShowHelp()
		Case $ButtonStep1ChooseFile
			SelectFile($sFileStep1ChooseM3Ufile)
			If $sThisNewFileHasBeenChosen = "None" Then
				MsgBox($MB_SYSTEMMODAL, "", "No file was selected, so nothing is changed.")
			Else
				GUICtrlSetData($LabelStep1ChosenFile, "Use_file:" & WrapTextInLabel($sThisNewFileHasBeenChosen, 43))
				$sFileStep1ChooseM3Ufile = $sThisNewFileHasBeenChosen
				IniWrite($sINIfile, "FilesAndFolders", "FileStep1ChooseM3Ufile", $sThisNewFileHasBeenChosen)
			EndIf
		Case $ButtonStep2ChooseFile
			SelectFile($sFileStep2GetLogos)
			If $sThisNewFileHasBeenChosen = "None" Then
				GUICtrlSetData($LabelStep2Status, "No file was selected, so nothing is changed.")
			Else
				GUICtrlSetData($LabelStep2ChosenFileAndFolder, WrapTwoLogoTextInLabel($sThisNewFileHasBeenChosen, $sFolderStep2SaveLogos, 43))
				$sFileStep2GetLogos = $sThisNewFileHasBeenChosen
				IniWrite($sINIfile, "FilesAndFolders", "FileStep2GetLogos_____", $sThisNewFileHasBeenChosen)
				GUICtrlSetData($LabelStep2Status, "File changed.")
			EndIf
		Case $ButtonStep3ChooseFile
			SelectFile($sFileStep3FixM3U)
			If $sThisNewFileHasBeenChosen = "None" Then
				GUICtrlSetData($LabelStep3Status, "No file was selected, so nothing is changed.")
			Else
				GUICtrlSetData($LabelStep3ChosenFile, "Use_file:" & WrapTextInLabel($sThisNewFileHasBeenChosen, 43))
				$sFileStep3FixM3U = $sThisNewFileHasBeenChosen
				IniWrite($sINIfile, "FilesAndFolders", "FileStep3FixM3U_______", $sThisNewFileHasBeenChosen)
				GUICtrlSetData($LabelStep3Status, "File changed.")
			EndIf
		Case $ButtonStep1ChooseDestination
			SelectFolder($sFolderStep1SaveGroup)
			If $sThisNewFolderHasBeenChosen = "None" Then
				GUICtrlSetData($LabelStep1Status, "No folder was selected, so nothing is changed.")
			Else
				GUICtrlSetData($LabelStep1ChosenFile, WrapTwoLogoTextInLabel($sFileStep1ChooseM3Ufile, $sThisNewFolderHasBeenChosen & "\", 43))
				$sFolderStep1SaveGroup = $sThisNewFolderHasBeenChosen & "\"
				IniWrite($sINIfile, "FilesAndFolders", "FolderStep1SaveGroup__", $sThisNewFolderHasBeenChosen & "\")
				GUICtrlSetData($LabelStep1Status, "Folder changed.")
			EndIf
		Case $ButtonStep2ChooseDestinationFolder
			SelectFolder($sFolderStep2SaveLogos)
			If $sThisNewFolderHasBeenChosen = "None" Then
				GUICtrlSetData($LabelStep2Status, "No folder was selected, so nothing is changed.")
			Else
				GUICtrlSetData($LabelStep2ChosenFileAndFolder, WrapTwoLogoTextInLabel($sFileStep2GetLogos, $sThisNewFolderHasBeenChosen & "\", 43))
				$sFolderStep2SaveLogos = $sThisNewFolderHasBeenChosen & "\"
				IniWrite($sINIfile, "FilesAndFolders", "FolderStep2SaveLogos__", $sThisNewFolderHasBeenChosen & "\")
				GUICtrlSetData($LabelStep2Status, "Folder changed.")
			EndIf
		Case $ButtonStep3ChooseFolder
			SelectFolder($sFolderStep3SaveFixM3U)
			If $sThisNewFolderHasBeenChosen = "None" Then
				MsgBox($MB_SYSTEMMODAL, "", "No file was selected, so nothing is changed.")
			Else
				GUICtrlSetData($LabelStep3ChosenFile, WrapTwoLogoTextInLabel($sFileStep3FixM3U, $sThisNewFolderHasBeenChosen & "\", 43))
				$sFolderStep3SaveFixM3U = $sThisNewFolderHasBeenChosen & "\"
				IniWrite($sINIfile, "FilesAndFolders", "FolderStep3FixM3U_____", $sThisNewFolderHasBeenChosen & "\")
				GUICtrlSetData($LabelStep3Status, "Folder changed.")
			EndIf
		Case $ButtonStep4DestinationFolder
			SelectFolder($sFolderStep4EPGXML)
			If $sThisNewFolderHasBeenChosen = "None" Then
				MsgBox(0, "No change", "Nothing chosen = nothing changed")
			Else
				GUICtrlSetData($LabelStep4Status, WrapTwoLogoTextInLabel($sFileStep4EPGXML & " (change in INI)", $sThisNewFolderHasBeenChosen, 42))
				$sFolderStep4EPGXML = $sThisNewFolderHasBeenChosen & "\"
				IniWrite($sINIfile, "FilesAndFolders", "FolderStep4EPGXML_____", $sThisNewFolderHasBeenChosen & "\")
			EndIf
		Case $CheckboxAutoChangeFolderFile
			IniWrite($sINIfile, "FilesAndFolders", "AutoFileNames_1_is_yes", GUICtrlRead($CheckboxAutoChangeFolderFile))
		Case $CheckboxMergeEPGXMLfiles
			IniWrite($sINIfile, "FilesAndFolders", "MergeEPGfiles_1_is_yes", GUICtrlRead($CheckboxMergeEPGXMLfiles))
		Case $ButtonStep1SaveToFile
			SaveOneGroupToFile()
		Case $ButtonStep2SaveToFolder
			Step2GetLogos()
		Case $ButtonStep3SaveFile
			Step3SaveFixedFile()
		Case $ButtonStep1GetGroups
			Step1GetGroups()
		Case $ButtonStep4GetAndMerge
			Step4GetAndMerge()
	EndSwitch
WEnd

Func main()

	If FileExists($sINIfile) = 0 Then

		Local $sWriteINItext, $sOpenINIfile
		$sWriteINItext = "[FilesAndFolders]" & @CRLF
		$sWriteINItext = $sWriteINItext & "FileStep1ChooseM3Ufile=" & @ScriptDir & "\tv_channels_plus.m3u" & @CRLF
		$sWriteINItext = $sWriteINItext & "FolderStep1SaveGroup__=" & @ScriptDir & "\" & @CRLF
		$sWriteINItext = $sWriteINItext & "AutoFileNames_1_is_yes=" & "1" & @CRLF
		$sWriteINItext = $sWriteINItext & "FileStep2GetLogos_____=" & @ScriptDir & "\tv_channels_plus.m3u" & @CRLF
		$sWriteINItext = $sWriteINItext & "FolderStep2SaveLogos__=" & @ScriptDir & "\logos\" & @CRLF
		$sWriteINItext = $sWriteINItext & "FileStep3FixM3U_______=" & @ScriptDir & "\tv_channels_plus.m3u" & @CRLF
		$sWriteINItext = $sWriteINItext & "FolderStep3FixM3U_____=" & @ScriptDir & "\tv_channels_plus.m3u" & @CRLF
		$sWriteINItext = $sWriteINItext & "FolderStep4EPGXML_____=" & @ScriptDir & "\" & @CRLF
		$sWriteINItext = $sWriteINItext & "MergeEPGfiles_1_is_yes=" & @ScriptDir & "4" & @CRLF
		$sWriteINItext = $sWriteINItext & "FileStep4EPGXML_______=" & "tvguide.xml" & @CRLF
		$sWriteINItext = $sWriteINItext & "" & @CRLF
		$sWriteINItext = $sWriteINItext & "[EPGXML]" & @CRLF
		$sWriteINItext = $sWriteINItext & "Url1=Write your links in the INI file " & @ScriptDir & "\" & $sINIfile & @CRLF
		$sWriteINItext = $sWriteINItext & "Url2=https://example.url.com/some/folder/and/file.xml"

		$sOpenINIfile = FileOpen($sINIfile, 1)
		If $sOpenINIfile = -1 Then
			MsgBox($MB_SYSTEMMODAL, "", "An error occurred when opening the INI file. Write protected? Using defaults...")
			Return False
		Else

			FileWrite($sOpenINIfile, $sWriteINItext)
			FileClose($sOpenINIfile)
			MsgBox(0, "New INI file", "I did not find a INI file, so I created it for you. You find it here: " & @CRLF & @ScriptDir & "\" & $sINIfile)

		EndIf

	EndIf

	;Get stuff from INI file to variables
	$sFileStep1ChooseM3Ufile = IniRead($sINIfile, "FilesAndFolders", "FileStep1ChooseM3Ufile", @ScriptDir & "\tv_channels_plus.m3u")
	$sFolderStep1SaveGroup = IniRead($sINIfile, "FilesAndFolders", "FolderStep1SaveGroup__", @ScriptDir & "\")
	$sFileStep2GetLogos = IniRead($sINIfile, "FilesAndFolders", "FileStep2GetLogos_____", @ScriptDir & "\tv_channels_plus.m3u")
	$sFolderStep2SaveLogos = IniRead($sINIfile, "FilesAndFolders", "FolderStep2SaveLogos__", @ScriptDir & "\logos\")
	$sFileStep3FixM3U = IniRead($sINIfile, "FilesAndFolders", "FileStep3FixM3U_______", @ScriptDir & "\tv_channels_plus.m3u")
	$sFolderStep3SaveFixM3U = IniRead($sINIfile, "FilesAndFolders", "FolderStep3FixM3U_____", @ScriptDir & "\")
	$sFolderStep4EPGXML = IniRead($sINIfile, "FilesAndFolders", "FolderStep4EPGXML_____", @ScriptDir & "\")
	$sFileStep4EPGXML = IniRead($sINIfile, "FilesAndFolders", "FileStep4EPGXML_______", "tvguide.xml")
	GUICtrlSetState($CheckboxAutoChangeFolderFile, IniRead($sINIfile, "FilesAndFolders", "AutoFileNames_1_is_yes", "1"))
	GUICtrlSetState($CheckboxMergeEPGXMLfiles, IniRead($sINIfile, "FilesAndFolders", "MergeEPGfiles_1_is_yes", "4"))

	GUICtrlSetData($LabelStep1ChosenFile, WrapTwoLogoTextInLabel($sFileStep1ChooseM3Ufile, $sFolderStep1SaveGroup, 43))
	GUICtrlSetData($LabelStep2ChosenFileAndFolder, WrapTwoLogoTextInLabel($sFileStep2GetLogos, $sFolderStep2SaveLogos, 43))
	GUICtrlSetData($LabelStep3ChosenFile, WrapTwoLogoTextInLabel($sFileStep3FixM3U, $sFolderStep3SaveFixM3U, 43))
	GUICtrlSetData($LabelStep4Status, WrapTwoLogoTextInLabel($sFileStep4EPGXML & " (change in INI)", $sFolderStep4EPGXML, 42))
	GUICtrlSetData($CheckboxMergeEPGXMLfiles, "Merge EPG XML files to " & $sFileStep4EPGXML)

	$aEPGXMLfiles = IniReadSection($sINIfile, "EPGXML")
	$aEPGXMLfiles[0][1] = "= Number of links"

	; Check if an error occurred.
	If Not @error Then
		; Enumerate through the array displaying the keys and their respective values.
		For $i = 1 To $aEPGXMLfiles[0][0]
			;MsgBox($MB_SYSTEMMODAL, "", "Key: " & $aEPGXMLfiles[$i][0] & @CRLF & "Value: " & $aEPGXMLfiles[$i][1])
		Next
	EndIf

	_GUICtrlListView_AddArray($ListViewEPGXMLfiles, $aEPGXMLfiles)

EndFunc   ;==>main

Func Step4GetAndMerge()

	; Step 1/3

	Local $hDownload
	Local $iBytesSize, $iFileSize, $sFilePath, $iFileSizeInfo
	Local $iSumFileSize = 0

	Local $TheINIfile = StringReplace(@ScriptName, StringRight(@ScriptName, 3), "ini")
	Local $sTheFolder = IniRead($TheINIfile, "FilesAndFolders", "FolderStep4EPGXML_____", @ScriptDir & "\")
	Local $sXMLfilename = IniRead($TheINIfile, "FilesAndFolders", "FileStep4EPGXML_______", "tvguide.xml")
	Local $aEPGXMLfiles = IniReadSection($TheINIfile, "EPGXML")
	Local $iMergeFiles = IniRead($TheINIfile, "FilesAndFolders", "MergeEPGfiles_1_is_yes", "4")
	Local $iAmount = UBound($aEPGXMLfiles)
	Local $sXMLTempfile = $sTheFolder & "\EpgTemp.txt"

	Local $sStatusInfo[3]

	; Step 2/3

	_FileWriteLog($sTheFolder & "EPGXML.log", "**************************************************")
	_FileWriteLog($sTheFolder & "EPGXML.log", "Started ")

	For $x = 1 To $iAmount - 1

		$sFilePath = $sTheFolder & "\Epg" & $x & ".xml"
		FileDelete($sFilePath)

		If $iSilentIs = 0 Then
			$sStatusInfo[2] = $sStatusInfo[1]
			$sStatusInfo[1] = $sStatusInfo[0]
			$sStatusInfo[0] = "Getting " & $x & "/" & $iAmount - 1 & " ..." & StringRight($aEPGXMLfiles[$x][1], 15)
			$sStatusTemp = $sStatusInfo[2] & @CRLF & $sStatusInfo[1] & @CRLF & $sStatusInfo[0]

			GUICtrlSetData($LabelStep4Status, $sStatusTemp)
		EndIf

		_FileWriteLog($sTheFolder & "EPGXML.log", " Getting    " & $aEPGXMLfiles[$x][1])
		$hDownload = InetGet($aEPGXMLfiles[$x][1], $sFilePath, $INET_FORCERELOAD, 1)

		; Wait for the download to complete by monitoring when the 2nd index value of InetGetInfo returns True.
		Do
			Sleep(250)
		Until InetGetInfo($hDownload, $INET_DOWNLOADCOMPLETE)

		; Retrieve the number of total bytes received and the filesize.
		$iBytesSize = InetGetInfo($hDownload, $INET_DOWNLOADREAD)
		$iFileSizeInfo = InetGetInfo($hDownload, $INET_DOWNLOADSIZE)
		$iFileSize = FileGetSize($sFilePath)
		$iSumFileSize = $iSumFileSize + $iFileSize

		_FileWriteLog($sTheFolder & "EPGXML.log", " Downloaded " & $iBytesSize & "/" & $iFileSizeInfo & " bytes")

		If $iSilentIs = 0 Then

			$sStatusInfo[0] = $sStatusInfo[0] & " > " & $iBytesSize & "/" & $iFileSizeInfo
			$sStatusTemp = $sStatusInfo[2] & @CRLF & $sStatusInfo[1] & @CRLF & $sStatusInfo[0]

			GUICtrlSetData($LabelStep4Status, $sStatusTemp)
		EndIf

		;ConsoleWrite("Www size: " & $iBytesSize & " file size: " & $iFileSize & " " & $sFilePath & " " & $sEPGXMLlink[$x] & @CRLF)

		; Close the handle returned by InetGet.
		InetClose($hDownload)
	Next

	; Step 3/3

	If $iMergeFiles = 1 Then

		Local $sOpenedFile
		Local $sTheFile = $sTheFolder & $sXMLfilename
		Local $sXMLTempfile = $sTheFolder & "\EpgTemp.txt"
		FileDelete($sXMLTempfile)

		If $iSilentIs = 0 Then
			$sStatusInfo[2] = $sStatusInfo[1]
			$sStatusInfo[1] = $sStatusInfo[0]
			$sStatusInfo[0] = "Merging"
			$sStatusTemp = $sStatusInfo[2] & @CRLF & $sStatusInfo[1] & @CRLF & $sStatusInfo[0]

			GUICtrlSetData($LabelStep4Status, $sStatusTemp)
		EndIf

		_FileWriteLog($sTheFolder & "EPGXML.log", "Downloaded in tot " & $iSumFileSize & " bytes. Starting merging files ")
		$sOpenedFile = FileOpen($sXMLTempfile, 1)

		FileWriteLine($sOpenedFile, "<?xml version=""1.0"" encoding=""UTF-8""?><tv>")

		Local $iTheCounter = 0
		For $x = 1 To $iAmount - 1

			$sFilePath = $sTheFolder & "Epg" & $x & ".xml"
			_FileWriteLog($sTheFolder & "EPGXML.log", " Merging " & $sFilePath)

			Local $aArray = FileReadToArray($sFilePath)
			If @error = 0 Then
				Local $iLineCount = @extended

;~ 		ConsoleWrite(UBound($aArray) & " = " & $sFilePath & @CRLF)

				$aArray[0] = StringReplace($aArray[0], "<?xml version=""1.0"" encoding=""UTF-8""?><tv>", "")
				$aArray[UBound($aArray) - 1] = StringReplace($aArray[UBound($aArray) - 1], "</tv>", "")

				For $i = 0 To UBound($aArray) - 1 ; Loop through the array. UBound($aArray) can also be used.
					FileWriteLine($sOpenedFile, $aArray[$i])

					$iTheCounter += 1
					If $iTheCounter > 2000 And $iSilentIs = 0 Then
						$sStatusInfo[0] = "Merging file " & $x & "/" & $iAmount - 1 & " row " & $i + 1 & "/" & UBound($aArray)
						$sStatusTemp = $sStatusInfo[2] & @CRLF & $sStatusInfo[1] & @CRLF & $sStatusInfo[0]
						GUICtrlSetData($LabelStep4Status, $sStatusTemp)
						$iTheCounter = 0
					EndIf
				Next
			Else
				_FileWriteLog($sTheFolder & "EPGXML.log", "!>> ERROR with " & $sFilePath)
			EndIf

		Next

		FileWriteLine($sOpenedFile, "</tv>")
		FileClose($sOpenedFile)

		If $iSilentIs = 0 Then

			$sStatusInfo[2] = $sStatusInfo[1]
			$sStatusInfo[1] = $sStatusInfo[0]
			$sStatusInfo[0] = "Merging done! "
			$sStatusTemp = $sStatusInfo[2] & @CRLF & $sStatusInfo[1] & @CRLF & $sStatusInfo[0]

			GUICtrlSetData($LabelStep4Status, $sStatusTemp)
		EndIf

		Local $iOldFileSize = FileGetSize($sTheFile)
		FileDelete($sTheFile)
		FileCopy($sXMLTempfile, $sTheFile, 1)
		$iFileSize = FileGetSize($sTheFile)
		;ConsoleWrite($iFileSize & " tot file size " & $iSumFileSize & @CRLF)
		_FileWriteLog($sTheFolder & "EPGXML.log", "Merging done, file size = " & $iFileSize & ", old size = " & $iOldFileSize)

	EndIf

	If $iSilentIs = 0 And $iMergeFiles = 4 Then

		$sStatusInfo[2] = $sStatusInfo[1]
		$sStatusInfo[1] = $sStatusInfo[0]
		$sStatusInfo[0] = "All done! "
		$sStatusTemp = $sStatusInfo[2] & @CRLF & $sStatusInfo[1] & @CRLF & $sStatusInfo[0]

		GUICtrlSetData($LabelStep4Status, $sStatusTemp)
	EndIf

	_FileWriteLog($sTheFolder & "EPGXML.log", "All done")
	FileCopy($sTheFolder & "EPGXML.log", @ScriptDir & "\EPGXML" & @ComputerName & ".log", 1)

	If $iSilentIs = 1 Then
		Exit
	EndIf

EndFunc   ;==>Step4GetAndMerge

Func Step3SaveFixedFile()

	Local $aArray = FileReadToArray($sFileStep3FixM3U)
	Local $iLineCount = @extended
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "", "There was an error reading the file. @error: " & @error) ; An error occurred reading the current script file.

	Else
		Local $aFixedArray[$iLineCount + 2]
		Local $iNewLineCount = 0
		Local $sGroupString
		Local $sFixedString
		$iSlowCounter = 0
		For $i = 0 To $iLineCount - 1 ; Loop through the array. UBound($aArray) can also be used.
			If StringCompare(StringLeft($aArray[$i], 8), "#EXTINF:") = 0 Then
				;Same
				If $i < $iLineCount Then

;~ 					$sGroupString = StringRight($aArray[$i], StringLen($aArray[$i]) - StringInStr($aArray[$i], "group-title=""") - 12)

;~ 					$sGroupString = StringLeft($sGroupString, StringInStr($sGroupString, """") - 1)

;~ 					$sGroupString = StringReplace($sGroupString, ":", "") & " - "

					;ConsoleWrite($sGroupString & @CRLF)

					;$sFixedString = "#EXTINF:0," & $sGroupString & StringRight($aArray[$i], StringLen($aArray[$i]) - StringInStr($aArray[$i], ","))
					$sFixedString = "#EXTINF:0," & StringRight($aArray[$i], StringLen($aArray[$i]) - StringInStr($aArray[$i], ","))

					ConsoleWrite($sFixedString & @CRLF)

					$aFixedArray[$iNewLineCount] = $sFixedString
					$iNewLineCount += 1

					$iSlowCounter += 1
					If $iSlowCounter > 50 Then
						$sStatusTemp = "Fixing " & $iNewLineCount
						GUICtrlSetData($LabelStep3Status, $sStatusTemp)
						$iSlowCounter = 0
					EndIf



				EndIf
			Else
				$aFixedArray[$iNewLineCount] = $aArray[$i]
				$iNewLineCount += 1
			EndIf

		Next

		Local $szDrive, $szDir, $szFName, $szExt, $sNewName
		Local $aPathSplit = _PathSplit($sFileStep3FixM3U, $szDrive, $szDir, $szFName, $szExt)

		$sNewName = $szDrive & $szDir & $szFName & "FIXED" & $szExt

		Local $hFileOpen = FileOpen($sNewName, $FO_OVERWRITE)

		$iSlowCounter = 0
		For $i = 0 To $iNewLineCount
			If StringCompare($aFixedArray[$i], "") = 0 And StringCompare($aFixedArray[$i + 1], "") = 0 Then
				;nothing
			ElseIf StringLen($aFixedArray[$i]) < 3 Then
				;nothing
			Else
				FileWriteLine($hFileOpen, $aFixedArray[$i])
				If @error Then
					MsgBox($MB_SYSTEMMODAL, "", "There was an error writing to the file. @error: " & @error) ; An error occurred reading the current script file.
				EndIf

				$iSlowCounter += 1
				If $iSlowCounter > 50 Then
					$sStatusTemp = "Writing " & $iNewLineCount
					GUICtrlSetData($LabelStep3Status, $sStatusTemp)
					$iSlowCounter = 0
				EndIf
			EndIf
		Next
		FileClose($hFileOpen)

		GUICtrlSetData($LabelStep3Status, "Writing: " & $iLineCount & " Alla done!")

	EndIf

EndFunc   ;==>Step3SaveFixedFile

Func Step2GetLogos()

	Local $aArray = FileReadToArray($sFileStep2GetLogos)
	Local $iLineCount = @extended
	If @error Then

		MsgBox($MB_SYSTEMMODAL, "", "There was an error reading the file. @error: " & @error)     ; An error occurred reading the current script file.

	Else
		Local $aFixedArray[$iLineCount + 2]
		Local $iNewLineCount = 0
		Local $sLogoString
		Local $sNameString
		Local $sFixedString
		Local $iBytesSize
		Local $iFileSize
		Local $sFilePath = $sFolderStep2SaveLogos
		Local $sFileToSave
		Local $hDownload

		If FileExists($sFilePath) Then
			; Do nothing
		Else
			DirCreate($sFilePath)
		EndIf



		For $i = 0 To $iLineCount - 1     ; Loop through the array. UBound($aArray) can also be used.
			If $i < $iLineCount Then

				If StringInStr($aArray[$i], "tvg-logo=""") > 1 Then

					$sLogoString = StringRight($aArray[$i], StringLen($aArray[$i]) - StringInStr($aArray[$i], "tvg-logo=""") - 9)

					$sLogoString = StringLeft($sLogoString, StringInStr($sLogoString, """") - 1)

					;ConsoleWrite($sLogoString & @CRLF)

					$sNameString = StringRight($aArray[$i], StringLen($aArray[$i]) - StringInStr($aArray[$i], ",")) & "." & StringRight($sLogoString, 3)

					$sFileToSave = $sFilePath & $sNameString

					GUICtrlSetData($LabelStep2Status, "Getting: " & Int(($i + 1) / 2) & "/" & Int($iLineCount / 2) & " " & $sNameString)
					;ConsoleWrite("Getting: " & Int(($i + 1) / 2) & "/" & Int($iLineCount / 2) & " " & $sNameString & @CRLF)

					$hDownload = InetGet($sLogoString, $sFileToSave, $INET_FORCERELOAD, $INET_DOWNLOADBACKGROUND)

					; Wait for the download to complete by monitoring when the 2nd index value of InetGetInfo returns True.
					Do
						Sleep(250)
					Until InetGetInfo($hDownload, $INET_DOWNLOADCOMPLETE)

					; Retrieve the number of total bytes received and the filesize.
					$iBytesSize = InetGetInfo($hDownload, $INET_DOWNLOADREAD)
					$iFileSize = FileGetSize($sFilePath & $sNameString)

					; Close the handle returned by InetGet.
					InetClose($hDownload)

					$aFixedArray[$iNewLineCount] = $sFixedString
					$iNewLineCount += 1
				EndIf
;~
			EndIf

		Next
		;ConsoleWrite("Getting: " & $sNameString & " = " & $iFileSize & " All done!" & @CRLF)
		GUICtrlSetData($LabelStep2Status, "Downloaded " & $iNewLineCount & " logos. All done!")

	EndIf

EndFunc   ;==>Step2GetLogos

Func SaveOneGroupToFile()

	Local $sSelectedGroup, $iSelectedGroupCount = 0
	For $y = 1 To UBound($aUniqueGroupsToListView) - 1
		If _GUICtrlListView_GetItemChecked($ListViewGroupSelection, $y - 1) Then
			$sSelectedGroup = _GUICtrlListView_GetItemText($ListViewGroupSelection, $y - 1, 1)
			ExitLoop
		EndIf
	Next

	If StringCompare($sSelectedGroup, "") = 0 Then

		GUICtrlSetData($LabelStep1Status, "Nothing chosen, so we did nothing.")

	ElseIf StringInStr($sSelectedGroup, "Choose only ONE!") > 1 Then

		GUICtrlSetData($LabelStep1Status, "Nothing chosen, so we did nothing.")

	Else
		ConsoleWrite("""" & $sSelectedGroup & """" & @CRLF)

		Local $aAllInfo, $sCheckLine, $iCountryGroupCounter = 0

		$aAllInfo = FileReadToArray($sFileStep1ChooseM3Ufile)

		If @error Then
			MsgBox($MB_SYSTEMMODAL, "", "There was an error reading the file. @error: " & @error)     ; An error occurred reading the current script file.
		Else

			GUICtrlSetData($LabelStep1Status, "Working...")

			Local $aCountryGroupInfo[UBound($aAllInfo)], $iSlowCounter = 25

			For $x = 0 To UBound($aAllInfo) - 1

				If StringInStr($aAllInfo[$x], "group-title=""" & $sSelectedGroup) > 0 Then

					$iCountryGroupCounter += 1
					$aCountryGroupInfo[$iCountryGroupCounter - 1] = $aAllInfo[$x]
					$iCountryGroupCounter += 1
					$aCountryGroupInfo[$iCountryGroupCounter - 1] = $aAllInfo[$x + 1]

					;ConsoleWrite($aCountryGroupInfo[$iCountryGroupCounter - 1] & @CRLF)

				EndIf

				$iSlowCounter += 1
				If $iSlowCounter > 500 Then
					$sStatusTemp = "1/2 Finding group " & $x & "/" & UBound($aAllInfo) - 1
					GUICtrlSetData($LabelStep1Status, $sStatusTemp)
					$iSlowCounter = 0
				EndIf

			Next

			Local $szDrive, $szDir, $szFName, $szExt
			Local $aPathSplit = _PathSplit($sFileStep1ChooseM3Ufile, $szDrive, $szDir, $szFName, $szExt)

			; Not valid in file name ['<', '>', '|', '"', '\', '/', ':', '*', '?']
			; [\x10-\x1F\x21-\x2F\x3A-\x40\x5B-\x60\x80-\xFF]
			Local $sGoodFilenamn = StringRegExpReplace($sSelectedGroup, "[^a-zA-Z0-9.]", "")

			ConsoleWrite($szDrive & $szDir & $sGoodFilenamn & $szExt & @CRLF)

			Local $hFileOpen = FileOpen($sFolderStep1SaveGroup & $sGoodFilenamn & $szExt, $FO_OVERWRITE)
			If @error Then
				MsgBox($MB_SYSTEMMODAL, "", "There was an error reading the file. @error: " & @error)     ; An error occurred reading the current script file.
			Else

				FileWriteLine($hFileOpen, "#EXTM3U")

				For $x = 0 To $iCountryGroupCounter - 1

					FileWriteLine($hFileOpen, $aCountryGroupInfo[$x])

					$iSlowCounter += 1
					If $iSlowCounter > 500 Then
						$sStatusTemp = "2/2 Saving group " & $x & "/" & $iCountryGroupCounter - 1
						GUICtrlSetData($LabelStep1Status, $sStatusTemp)
						$iSlowCounter = 0
					EndIf


				Next

				$sStatusTemp = "2/2 Saving group " & $x & "/" & $iCountryGroupCounter
				FileClose($hFileOpen)

				If GUICtrlRead($CheckboxAutoChangeFolderFile) = 1 Then
					ConsoleWrite("Auto = yes" & @CRLF)
					; Step 2 file
					$sFileStep2GetLogos = $sFolderStep1SaveGroup & $sGoodFilenamn & $szExt
					IniWrite($sINIfile, "FilesAndFolders", "FileStep2GetLogos_____", $sFileStep2GetLogos)
					GUICtrlSetData($LabelStep2Status, "File auto changed")
					GUICtrlSetData($LabelStep2ChosenFileAndFolder, WrapTwoLogoTextInLabel($sFileStep2GetLogos, $sFolderStep2SaveLogos, 43))
					; Step 2 folder
					$sFolderStep2SaveLogos = $sFolderStep1SaveGroup & $sGoodFilenamn & "logos\"
					IniWrite($sINIfile, "FilesAndFolders", "FileStep2GetLogos_____", $sFileStep2GetLogos)
					GUICtrlSetData($LabelStep2Status, "File & folder auto changed")
					GUICtrlSetData($LabelStep2ChosenFileAndFolder, WrapTwoLogoTextInLabel($sFileStep2GetLogos, $sFolderStep2SaveLogos, 43))
					; Step 3
					$sFileStep3FixM3U = $sFolderStep1SaveGroup & $sGoodFilenamn & $szExt
					IniWrite($sINIfile, "FilesAndFolders", "FileStep3FixM3U_______", $sFileStep3FixM3U)
					GUICtrlSetData($LabelStep3Status, "File auto changed")
					GUICtrlSetData($LabelStep3ChosenFile, WrapTwoLogoTextInLabel($sFileStep3FixM3U, $sFolderStep3SaveFixM3U, 43))
				EndIf
			EndIf
		EndIf

		GUICtrlSetData($LabelStep1Status, $sStatusTemp & " All done!")

	EndIf



EndFunc   ;==>SaveOneGroupToFile

Func Step1GetGroups()

	Local $aAllInfo, $sCheckLine, $iCountryGroupCounter = 0

	$aAllInfo = FileReadToArray($sFileStep1ChooseM3Ufile)
	If @error Then
		MsgBox($MB_SYSTEMMODAL, "", "There was an error reading the file. @error: " & @error)     ; An error occurred reading the current script file.
	Else

		GUICtrlSetData($LabelStep1Status, "Working...")

		Local $aCountryGroupInfo[UBound($aAllInfo)], $iSlowCounter = 25

		For $x = 0 To UBound($aAllInfo) - 1

			If StringInStr($aAllInfo[$x], "group-title=""") > 0 Then

				$sCheckLine = StringRight($aAllInfo[$x], StringLen($aAllInfo[$x]) - StringInStr($aAllInfo[$x], "group-title=""") + 1)
				$sCheckLine = StringTrimLeft($sCheckLine, 13)
				$sCheckLine = StringLeft($sCheckLine, StringInStr($sCheckLine, """") - 1)

				$iCountryGroupCounter += 1
				$aCountryGroupInfo[$iCountryGroupCounter - 1] = $sCheckLine

				;ConsoleWrite($aCountryGroupInfo[$iCountryGroupCounter - 1] & @CRLF)

			EndIf

			$iSlowCounter += 1
			If $iSlowCounter > 500 Then
				$sStatusTemp = "Working " & $x & "/" & UBound($aAllInfo) - 1
				GUICtrlSetData($LabelStep1Status, $sStatusTemp)
				$iSlowCounter = 0
			EndIf

		Next
		$sStatusTemp = "Working " & UBound($aAllInfo) - 1 & "/" & UBound($aAllInfo) - 1

		Global $aUniqueCountryGroups = _ArrayUnique($aCountryGroupInfo)
		$aUniqueCountryGroups[0] = "Tot " & $aUniqueCountryGroups[0] & " groups. Choose only ONE! = 1 (not more)"

		ReDim $aUniqueGroupsToListView[UBound($aUniqueCountryGroups)][2]

		For $x = 0 To UBound($aUniqueCountryGroups) - 1

			$aUniqueGroupsToListView[$x][0] = $x
			$aUniqueGroupsToListView[$x][1] = $aUniqueCountryGroups[$x]

		Next

		$sStatusTemp = $sStatusTemp & " All done!"
		GUICtrlSetData($LabelStep1Status, $sStatusTemp)

		_GUICtrlListView_DeleteAllItems($ListViewGroupSelection)
		_GUICtrlListView_AddArray($ListViewGroupSelection, $aUniqueGroupsToListView)

	EndIf

EndFunc   ;==>Step1GetGroups

Func SelectFolder($sStartFolder)

	; Create a constant variable in Local scope of the message to display in FileOpenDialog.
	Local Const $sMessage = "Select a folder"
	Local $szDrive, $szDir, $szFName, $szExt

	_PathSplit($sStartFolder, $szDrive, $szDir, $szFName, $szExt)

	; Display an open dialog to select a file.
	Local $sFolderOpenDialog = FileSelectFolder($sMessage, $szDrive & $szDir)
	If @error Then
		; Display the error message.
		;MsgBox($MB_SYSTEMMODAL, "", "No folder was selected, so nothing is changed.")
		; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
		FileChangeDir(@ScriptDir)
		$sThisNewFolderHasBeenChosen = "None"
	Else
		; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
		FileChangeDir(@ScriptDir)
		; Save the selected file location
		$sThisNewFolderHasBeenChosen = $sFolderOpenDialog
	EndIf

EndFunc   ;==>SelectFolder

Func SelectFile($sStartFolder)

	; Create a constant variable in Local scope of the message to display in FileOpenDialog.
	Local Const $sMessage = "Select a file"
	Local $szDrive, $szDir, $szFName, $szExt

	_PathSplit($sStartFolder, $szDrive, $szDir, $szFName, $szExt)

	; Display an open dialog to select a file.
	Local $sFileOpenDialog = FileOpenDialog($sMessage, $szDrive & $szDir, "MP3 URL (*.m3u)", $FD_FILEMUSTEXIST)
	If @error Then
		; Display the error message.
		;MsgBox($MB_SYSTEMMODAL, "", "No file was selected, so nothing is changed.")
		; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
		FileChangeDir(@ScriptDir)
		$sThisNewFileHasBeenChosen = "None"
	Else
		; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
		FileChangeDir(@ScriptDir)
		; Save the selected file location
		$sThisNewFileHasBeenChosen = $sFileOpenDialog
	EndIf

EndFunc   ;==>SelectFile

Func WrapTextInLabel($sTheLabelText, $sNumOfChars)

	Local $sWrappedText
	$sWrappedText = StringLeft($sTheLabelText, $sNumOfChars)
	$sWrappedText = $sWrappedText & @LF
	$sWrappedText = $sWrappedText & StringTrimLeft($sTheLabelText, $sNumOfChars)

	Return $sWrappedText

EndFunc   ;==>WrapTextInLabel

Func WrapTwoLogoTextInLabel($sTheLabelTextFile, $sTheLabelTextFolder, $sNumOfChars)

	Local $sWrappedText
	$sWrappedText = "Use_file:" & StringLeft($sTheLabelTextFile, $sNumOfChars)
	$sWrappedText = $sWrappedText & @LF
	$sWrappedText = $sWrappedText & StringTrimLeft($sTheLabelTextFile, $sNumOfChars) & @LF
	$sWrappedText = $sWrappedText & "Save_folder:" & StringLeft($sTheLabelTextFolder, $sNumOfChars)
	$sWrappedText = $sWrappedText & @LF
	$sWrappedText = $sWrappedText & StringTrimLeft($sTheLabelTextFolder, $sNumOfChars)

	Return $sWrappedText

EndFunc   ;==>WrapTwoLogoTextInLabel

Func ShowHelp()

	ShellExecute("https://gitlab.com/MrJackG/iptvtomp1/")

EndFunc   ;==>ShowHelp

Func ShowAbout()

	MsgBox(0, "About", "Software and source shared as GNU GPLv3")

EndFunc   ;==>ShowAbout


