# IPTVtoMP1

IPTVtoMP1-tools.exe is a tool that may help you to get IPTV streams to work on Media Portal 1 (see https://www.team-mediaportal.com/ )

But you can also do it manually. In the following text both the manual process and the use of IPTVtoMP1-tools.exe is explained. 
I would say that the most tiresome step to do manually is to download logos (icons) for TV channels.

**********************************************************
Step 1 – Choose what group of channels you will add to Media Portal 1
**********************************************************

=== POLARBEAR ===

First you have to get a M3U file with all the channels. From that file you will the extract one group to add to MP1 (Media Portal 1), for example the group “Spanish channels”. As you will see in the following steps, it will make it MUCH easier to add channels if you do it group by group.

* A) Login into Polarbear with Chrome (it works best, Firefox has problems).
* B) In the menu at the left side, choose “`Manage Lines`”
* C) Under the column header “`Opt`”, click on “`...`” and then on “`Download`”
* D) On the row for “`m3u With Options`”, click on `MPEGTS`, copy the link on the right and paste it in a new tab in Chrome to download it.
* E) Now you should have a file with a name similar to `tv_channels_MyUserName_plus.m3u`


Now you will choose one group of streams that you will add. 

You can do it manually or use the tool IPTVtoMP1-tools.exe.

=== PREPARE MANUALLY ===
* A) Open the file “tv_channels_MyUserName_plus.m3u” with a text editor, maybe Notepad, Notepad2 or Notepad++. You will see rows similar to this:
```
#EXTINF:-1 tvg-id="DisneyChannel.es" tvg-name="Discovery Channel ES" tvg-logo="https://cdn-static.ams3.cdn.digitaloceanspaces.com/picons/discoverychannel.png" group-title="LIVE: Spain",Discovery Channel ES 
http://tv.polar.tv.com:80/live/username/Abcd1234/12345.ts
```
* B) Both rows belong together. The first have info about the
 stream, and the second is the stream. In the first you see `group-title="LIVE: Spain"`, that is the group we will get in this tutorial. Copy all `#EXTINF` rows and associated streams, that have `group-title="LIVE: Spain”`, to a new file. Let’s save the file as `LIVESpain.m3u`. Make sure you have `#EXTM3U` alone in the first row (see original file for how it should look).

=== PREPARE WITH TOOL ===
If it is the first time you use the tool, you need to choose files and folders. Next time it will have remembered your choices. 
* A) Start IPTVtoMP1-tools.exe and start with “Step 1 – Choose one group of channels”
* B) Click the button “Choose M3U File” and choose `tv_channels_MyUserName_plus.m3u`
* C) Click the button “Get groups” and then check the checkbox for “LIVE: Spain”
* D) Click the button “Choose Destination Folder”, as a suggestion you can choose the same folder as where the file `tv_channels_MyUserName_plus.m3u` is.
* E) Click the button “Save group”. The tool will now save a file with all row with `group-title="LIVE: Spain"` and associated streams to the file `LIVESpain.m3u`


************************************************************

Step 2 – Get logos for channels, you will then add them to Media Portal 1

************************************************************

Now you will get logos for the channels. When you later use the channel guide in MP1, instead of nothing you will see the logo of the channel. 
For more info, see:

https://www.team-mediaportal.com/wiki/display/MediaPortal1/Channel-Logos

You can do it manually or use the tool IPTVtoMP1-tools.exe.

=== PREPARE MANUALLY ===

* A) Open the file “LIVESpain.m3u” with a text editor, maybe Notepad, Notepad2 or Notepad++. You will see rows similar to this: 

`#EXTINF:-1 tvg-id="DiscoveryChannel.es" tvg-name="Discovery Channel ES" tvg-logo="https://cdn-static.ams3.cdn.digitaloceanspaces.com/picons/discoverychannel.png" group-title="LIVE: Spain",Discovery Channel ES `

The URL “https://cdn-static.ams3.cdn.digitaloceanspaces.com/picons/discoverychannel.png” is the link to the logo.

The last text in the row, “`Discovery Channel ES`”,  will be both channel name and logo name

* A) Download the logo, for example https://cdn-static.ams3.cdn.digitaloceanspaces.com/picons/discoverychannel.png
* B) Give it a name associated with the channel name, for example `Discovery Channel ES.png`.
* C) Do this for all channels.


=== PREPARE WITH TOOL ===

If it is the first time you use the tool, you need to choose files and folders. Next time it will have remembered your choices. Also, if you have “Auto Files in steps 2 and 3” ticked, it will choose the latest created file in steps 2 and 3.
* A) Start IPTVtoMP1-tools.exe and start with “`Step 2 – Get logos for channels`”
* B) Click the button “`Choose M3U File`” and choose “`LIVESpain.m3u`”
* C) Click the button “`Choose Destination Folder`”. As a suggestion you can create the folder “`logos`” in folder where the file “`tv_channels_MyUserName_plus.m3u`” is, and choose it.
* E) Click the button “Get logos”. It will get all logos in the file and give them the same name as the channel.


=== MEDIA PORTAL 1 ===

Copy all logos to:
`C:\ProgramData\Team MediaPortal\MediaPortal\Thumbs\tv\logos`

************************************************************

Step 3 – Fix M3U to Media Portal 1 format

************************************************************

Now you will fix the “LIVESpain.m3u” so MP1 can scan in. It doesn’t understand the extra info in the M3U file, so we need to clean in. 

For more info, see:
1. https://www.team-mediaportal.com/wiki/display/MediaPortal1/Scan+DVB+-+IP

2. https://www.team-mediaportal.com/wiki/display/MediaPortal1/Supported+URL+formats+and+examples


You can do it manually or use the tool IPTVtoMP1-tools.exe.

=== PREPARE MANUALLY ===

There are two ways.

1) Get a new file with less info and delete all channels except for “Live: Spain” ones.

* A) Login into Polarbear with Chrome (it works best, Firefox has problems).
* B) In the menu at the left side, choose “`Manage Lines`”
* C) Under the column header “`Opt`”, click on “`...`” and then on “`Download`”
* D) On the row for “`m3u`”, click on `MPEGTS`, copy the link on the right and paste it in a new tab in Chrome to download it.
* E) Now you should have a file with a name similar to `tv_channels_MyUserName.m3u`. Open the file with a text editor, maybe Notepad, Notepad2 or Notepad++. You will see rows similar to this:

```
#EXTINF:-1,Discovery Channel ES
http://tv.polar.tv.com:80/live/username/Abcd1234/12345.ts
```

The both rows belong together. The first have info about the stream, and the second is the stream. In the first you see it ends with ES, that is the “group” we will use in this tutorial. Copy all #`EXTINF` rows and associated streams, that ends with ES, to a new file. Let’s save the file as `LIVESpainFIXED.m3u`. Make sure you have #`EXTM3U` alone in the first row (see original file for how it should look).

2) Clean the file `LIVESpain.m3u` you made in Step 2.


* A) Open the file “`LIVESpain.m3u`” with a text editor, maybe Notepad, Notepad2 or Notepad++. You will see rows similar to this: 

`#EXTINF:-1 tvg-id="DiscoveryChannel.es" tvg-name="Discovery Channel ES" tvg-logo="https://cdn-static.ams3.cdn.digitaloceanspaces.com/picons/discoverychannel.png" group-title="LIVE: Spain",Discovery Channel ES `

You will only keep the beginning:
`#EXTINF:-1`

And the end from the comma , :
`,Discovery Channel ES`

So it looks like this:
`#EXTINF:-1, Discovery Channel ES`

You could use Notepad++ and do this:

* B) Choose menu Search – Replace
* C) Under “Search mode” select “Regular expression”
* D) As “Find what:” write
* `.+(\,)`
* E) As “Replace with:” write 
* `\1`
* F) Click the button “Replace all”.
* G) Under “Search mode” select “Normal”
* H) As “Find what:” write
* `,`
* I) As “Replace with:” write 
* `#EXTINF:-1,`
* J) Click the button “Replace all”.
* K) Let’s save the file as `LIVESpainFIXED.m3u`.


=== PREPARE WITH TOOL ===

If it is the first time you use the tool, you need to choose files and folders. Next time it will have remembered your choices. Also, if you have “Auto Files in steps 2 and 3” ticked, it will choose the latest created file in steps 2 and 3.
* A) Start IPTVtoMP1-tools.exe and start with “`Step 3 – Fix M3U to MP1 format`”
* B) Click the button “`Choose M3U File`” and choose `LIVESpain.m3u`
* C) Click the button “`Choose Destination Folder`”, as a suggestion you can choose the same folder as where the file `tv_channels_MyUserName_plus.m3u` is.
* E) Click the button “`Fix/Save`”. It will clean the file and save it as `LIVESpainFIXED.m3u`.


=== MEDIA PORTAL 1 ===

Now we will add the channels to MP1

* A) Copy the file “`LIVESpainFIXED.m3u`” to:
* `C:\ProgramData\Team MediaPortal\MediaPortal TV Server\TuningParameters\dvbip`
* B) Start `TV-Server Configuration`

C) Go to `TV Servers – ComputerName – 2 DVB-IP MediaPortal IPTV Source Filter`

If you don’t find it, in Start `TV-Server Configuration`, click on `Plugins`. Check `Media Portal IPTV filter and url source splitter`. Then restart TV-server.
* D) As `Service:`, choose `LIVESpainFIXED`
* E) Click the button `Scan for channels`. This will be VERY slow…
* F) When done, go to `TV Channels`.
* G) Under `Channel groups`, click the button `Add` and write for example `Spain`.
* H) Select all Spanish channels, should be all channels without a group, right click on selection and choose Add to group - Spain. You can sort the list by clicking on the column headers. If you use groups it will be much easier later when you add EPG.

************************************************************

Step 4 – Get and merge EPG XML files

************************************************************

There are many different ways to get EPG (electronic program guide) in Media Portal 1.

1) WebEPG plugin that try to download EPG from different sites.
2) XmlTv plugin that uses a XML file to import EPG.
3) WebGrab+Plus is an application and a community for EPG, they have MANY sources!
4) EPG Buddy application that can use both websites and XML files.

It is quite hard work to get EPS to all channels, so it is best to use a few ways, I use 1, 2 and 3. Even then, you may have special channels without EPG. 

For more info, see:
1. https://www.team-mediaportal.com/wiki/display/MediaPortal1/WebEPG+Plugin
2. https://www.team-mediaportal.com/wiki/display/MediaPortal1/WebEPG
3. https://www.team-mediaportal.com/wiki/display/MediaPortal1/XmlTv+Plugin
4. https://www.team-mediaportal.com/wiki/display/MediaPortal1/XMLTV+GUI
5. https://www.team-mediaportal.com/wiki/display/MediaPortal1/XMLTV
6. https://www.team-mediaportal.com/extensions/tools/epg-buddy
7. https://forum.team-mediaportal.com/threads/epg-buddy-a-new-epg-tool-with-easy-usage.135484/
8: http://webgrabplus.com/

=== MEDIA PORTAL 1, WebEPG plugin ===

To enable plugin:
- In TV-Server Configuration, go to “Plugins” and check WebEPG. Restart TV-server.

Mapp channels
* A) In TV-Server Configuration, go to `Plugins` – `WebEPG`
* B) Click tab “`TV Mappings`”. As ”`Group:`” you choose “`Spain`” and click “`Import`”
* C) Click tab “`General`”. As ”`Country`” you choose “`Spain`” and click “`Auto Map`”
* D) Click tab “`TV Mappings`”. Click on a row where “`Grapper`” is empty. 
* E) Then, under “`Mapping Details`” at the end of “`Channel`”, click on the button “`...`”
* F) Click “`+`” in front of “`Web Sites`”, “`+`” in front of “`Spain`” and “`+`” in front of one EPG site.
* G) If you find the channel, click it and the click the button “`Select`”.
* H) Keep the “`Selection`” dialog open, and click next empty “`Grabber`” row. 
* I) Do this for all empty rows, probably you have 30% chance of finding a channel.
* J) Click tab “`General`” and click “`Save`”**!!!**
* K) To update EPG when you are done, click tab “`General`” and click “`Grab now!`”.


Schedule auto update
* A) In `TV-Server Configuration`, go to “`Plugins`” – “`WebEPG`”
* B) Click tab “`Schedule`”.
* C) One suggestion is to choose time “`03:00`”, and check all days.


=== MEDIA PORTAL 1, XmlTv plugin ===

To enable plugin:
- In TV-Server Configuration, go to “Plugins” and check XmlTv. Restart TV-server.

Get EPG XML files manually
* A) Login into Polarbear with Chrome (it works best, Firefox has problems).
* B) In the menu at the left side, choose “`Channels`”
* C) In the top menu, click `EPG`
* D) Scroll down to Spain, right click on `.XML` and choose “`Save link as…`” and save the file “`spain.xml`” to:
* `C:\ProgramData\Team MediaPortal\MediaPortal TV Server\xmltv`
* E) Create the file “`tvguide.lst`” in the same folder.
* F) Open “`tvguide.lst`” with Notepad (or similar) in one own line, add
* `spain.xml`

Here you can later add more XML files, on their own line.

Get EPG XML files automatically with tool
* A) Login into Polarbear with Chrome (it works best, Firefox has problems).
* B) In the menu at the left side, choose “`Channels`”
* C) In the top menu, click `EPG`
* D) Scroll down to Spain, right click on `.XML` and copy Link address
* E) It can be easier to have one copy of IPTVtoMP1-tools.exe in folder:
* `C:\ProgramData\Team MediaPortal\MediaPortal TV Server\xmltv`
* F) Open IPTVtoMP1-tools.ini (if you don’t have the INI file, start IPTVtoMP1-tools.exe once and it will create a INI file in the same folder as the EXE file) and paste the link on an own row under [EPGXML], maybe something like this:
```
[EPGXML]
Url1=https://polar.com/epg/spain.xml
```
* G) Create the file “`tvguide.lst`” in the same folder.
* H) Open “`tvguide.lst`” with Notepad (or similar) in one own line, add
* `Epg1.xml`

Here you can later add more XML files, on their own line. The next will be Epg2.xml and so on.
* I) Start IPTVtoMP1-tools.exe and start with “`Step 4 – Get and merge EPG XML files`” (do NOT use “Merge EPG XML files to tvguide.xml”)
* J) Click the button “Choose Destination folder” and choose:
* `C:\ProgramData\Team MediaPortal\MediaPortal TV Server\xmltv`
* K) To test, click “`Get/Merge EPG XML`” button.
* L) To run automatically, open Windows standard application “`Task Scheduler`”
* M) Click “`Create Basic Task…`”
* N) Name: `IPTVXML Update EPG` --> Next
* O) `Daily` --> Next
* P) `Start 03:10, Recur every 1 days `--> Next
* Q) `Start a program` --> Next
* R) Program/script = `C:\ProgramData\Team MediaPortal\MediaPortal TV Server\xmltv\ IPTVtoMP1-tools.exe`
* Add arguments = `/epg`
* Then click Next
* S) Click “`Finish`”

Next time you add a new XML file, you add the URL to IPTVtoMP1-tools.ini and one more Epg-.xml in tvguide.lst, and it will update also the new one automatically.

In Media Portal
* A) In `TV-Server Configuration`, go to “`Plugins`” – “`XmlTv`”
* B) In tab “`General`”, un-check “`Import new tvguide.xml`”
* C) In tab “`General`”, check “`Import files in new tvguide.lst`”
* D) Click “`OK`” and restart TV-Server Configuration, go to “`Plugins`” – “`XmlTv`”
* E) In tab “`General`”, click on “`Import`” button. If it seems like it is frozen and has hung, it is just working. It may take some time… A lot of time… If you click the tab “`Mappings`” (nothing happens), you can go and get some coffee, when it suddenly opens the tab you know it is done ;)
* F) In tab “`Mappings`”, as “`Group:`” choose “`Spain`” and click “`Load/Refresh`” button.
* G) Click on a row where “`Guide channel`” is empty. Use the combobox and try to find a channel. 
* Or click and then write the first letters to make it faster. If it found wrong, press Delete on keyboard to clear.
* H) Do this for all empty rows, probably you have 30% chance of finding a channel.
* I) Click “`Save`”**!!!** (In tab “`Mappings`”)


=== MEDIA PORTAL 1, WebGrab+Plus application ===

If you first setup WebEPG, maybe even XmlTv, this will go faster.

First, to know what sites you want later in the process (those that are missing in EPG in MP1), go to http://webgrabplus.com/epg-channels and click on Spain. 

Here you click on the “name”, for example “elmundo.es”, and check if the missing channels in XmlTv you want is there. You can also search on the page (Ctrl + f) after your channels. It may be that you will not find some channels. 

Note the site names & what channels you want from the sites! You will need this later…

* A)Go to http://webgrabplus.com/download , download and install WebGrabPlus
* I did go to http://webgrabplus.com/download/sw and downloaded latest beta
* B) Go to WebGrabPlus configuration folder, something like
* `C:\Users\admin\AppData\Local\WebGrab+Plus`
* Or try
* `%LOCALAPPDATA%/Webgrab+Plus`
* C) Download a new site.ini pack from http://webgrabplus.com/epg-channels and replace the one you have.
* D) Open the` WebGrab++.config.xml` file with Notepad
* E) Delete this dummy site:
* `<channel site="dummy" site_id="xx" update="i" xmltv_id="Example">Example</channel>`
* F) Check your notes. Go to `C:\Users\admin\AppData\Local\WebGrab+Plus\siteini.pack\Spain` and open the first site file with Notepad, maybe `sincroguia.tv.2d.channels.xml` (do not change these files)
* G) Check your notes. Copy the `<channel update=` row for all channels to `WebGrab++.config.xml` (where the dummy was). One tip is to use the search function when you search for a channel.
* H) Open more site files and continue copy channels to `WebGrab++.config.xml`
* I) Test by running `WG++` (maybe from `C:\Program Files (x86)\WebGrab+Plus\bin\WebGrab+Plus.exe` or from the Start menu)
* J) It should have created a `guide.xml` filen in `%LOCALAPPDATA%/Webgrab+Plus`
* K) Check `%LOCALAPPDATA%/Webgrab+Plus\WebGrab++.log.txt` for errors. If some site don’t work, you need to use another one.
* L) In `C:\ProgramData\Team MediaPortal\MediaPortal TV Server\xmltv`
* Open (or create) `tvguide.lst` with Notepad (or similar) in one own line, add
* `C:\Users\superadmin\AppData\Local\WebGrab+Plus\guide.xml`


Automatically run WG++ (and update guide.xml)
* A) To run automatically, open Windows standard application “`Task Scheduler`”
* B) Click “`Create Basic Task…`”
* C) Name: `WebGrab Update EPG` --> Next
* D) `Daily` --> Next
* E) `Start 03:30, Recur every 1 days` --> Next
* F) `Start a program` --> Next
* R) Program/script = `C:\Program Files (x86)\WebGrab+Plus\bin\WebGrab+Plus.exe`
* Then click `Next`
* G) Click “`Finish`”


In Media Portal
* A) In `TV-Server Configuration`, go to “`Plugins`” – “`XmlTv`”
* B) In tab “`General`”, un-check “`Import new tvguide.xml`”
* C) In tab “`General`”, check “`Import files in new tvguide.lst`”
* D) Click “`OK`” and restart TV-Server Configuration, go to “`Plugins`” – “`XmlTv`”
* E) In tab “`General`”, click on “`Import`” button. If it seems like it is frozen and has hung, it is just working. It may take some time… A lot of time… If you click the tab “`Mappings`” (nothing happens), you can go and get some coffee, when it suddenly opens the tab you know it is done ;)
* F) In tab “`Mappings`”, as “`Group:`” choose “`Spain`” and click “`Load/Refresh`” button.
* G) Click on a row where “`Guide channel`” is empty. Use the combobox and try to find a channel. 
* Or click and then write the first letters to make it faster. If it found wrong, press Delete on keyboard to clear.
* H) Click “`Save`”**!!!** (In tab “`Mappings`”)



=== MEDIA PORTAL 1, EPG Buddy application ===

If you want it to also get EPG XML links
* A) Login into Polarbear with Chrome (it works best, Firefox has problems).
* B) In the menu at the left side, choose “`Channels`”
* C) In the top menu, click `EPG`
* D) Scroll down to Spain, right click on `.XML` and copy Link address (maybe paste to Notepad, until we can save it in EPG Buddy)


Now we can setup EPG Buddy
* A) Install `EPG Buddy` and then start it
* B) Click on “`Setup TV database`” and check` MP1/2 MySQL` (if you don’t have some special setup), and click “`Save`”
* C) Click on “`EPG Source`” and check “`WebGrab+Plus`”. 
* D) Also check “`External XML`”, and paste the EPG XML link from Polarbear in the “`URL/Path`” text box. Then click “`Save`”
* E) Click “`EPG Data`” – “`WebGrab+Plus`”
* F) Choose “`Database mode`”, then click again on “`WebGrab+Plus`”
* G) Click “`Start`”. Choose some Websites. Maybe calle12.es, canalhistoria.es, canalpluse-export.es and some more that ends with .es (=Spain). The click “`Set`”.
* H) Under “`Website-selection`”, choose one site and click “`Show`”


Now you have at least started the configuration, now see the guide here for more info:
https://forum.team-mediaportal.com/threads/epg-buddy-a-new-epg-tool-with-easy-usage.135484/

